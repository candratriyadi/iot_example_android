package candra.com.iotexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback
import com.amazonaws.mobile.client.AWSMobileClient
import kotlinx.android.synthetic.main.activity_main.*
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset


class MainActivity : AppCompatActivity() {

    val LOG_TAG = "MainActivity"
    val mqttManager = AWSIotMqttManager(
        "AKIAIVWDMCSFMKW3RGMA",
        "a1sm5n1r7dws4l-ats.iot.us-west-2.amazonaws.com"
    )

    val topic = "/pydt/dev/user/6f72f7c7-b0c8-410e-866a-4307f97cac07/orders"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        connectIOT()
    }

    fun connectIOT(){



        try {
            mqttManager.connect(
                AWSMobileClient.getInstance()
            ) { status, throwable ->
                txt_test.text = "Connection Status: $status"
                Log.d(LOG_TAG, "Connection Status: $status")
            }
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Connection error: ", e)
        }

    }

    fun subscribe (){
        try {
            mqttManager.subscribeToTopic(topic, AWSIotMqttQos.QOS0 /* Quality of Service */,
                AWSIotMqttNewMessageCallback { topic, data ->
                    try {
                        val message = String(data, Charset.forName("UTF-8"))
                        Log.d(LOG_TAG, "Message received: $message")
                    } catch (e: UnsupportedEncodingException) {
                        Log.e(LOG_TAG, "Message encoding error: ", e)
                    }
                })
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Subscription error: ", e)
        }

    }
}
