package candra.com.iotexample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.amazonaws.mobile.client.AWSMobileClient;

import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

public class NewActivity extends AppCompatActivity {

    TextView text, txtMessage;
    AWSIotMqttManager mqttManager;
    String clientId;

    String topicMatching = "/pydt/demo/user/6f72f7c7-b0c8-410e-866a-4307f97cac07/orders";
    String topicUpdatedlist = "/pydt/demo/user/6f72f7c7-b0c8-410e-866a-4307f97cac07/get_user_orders";
//    String topicUpdatedlist = "/pydt/demo/user/51e99535-2a77-4504-b9b6-6f7672395757/get_user_orders";
    String LOG_TAG = "Candra";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.txt_test);
        txtMessage = findViewById(R.id.txt_message);

        clientId = UUID.randomUUID().toString();

        // Initialize the credentials provider
        final CountDownLatch latch = new CountDownLatch(1);
        AWSMobileClient.getInstance().initialize(
                getApplicationContext(),
                new Callback<UserStateDetails>() {
                    @Override
                    public void onResult(UserStateDetails result) {
                        latch.countDown();
                    }

                    @Override
                    public void onError(Exception e) {
                        latch.countDown();
                        Log.e(LOG_TAG, "onError: ", e);
                    }
                }
        );

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // MQTT Client
        mqttManager = new AWSIotMqttManager(clientId, "a1sm5n1r7dws4l-ats.iot.us-west-2.amazonaws.com");

        try {
            mqttManager.connect(AWSMobileClient.getInstance(), new AWSIotMqttClientStatusCallback() {
                @Override
                public void onStatusChanged(final AWSIotMqttClientStatus status,
                                            final Throwable throwable) {
                    Log.e(LOG_TAG, "Status = " + String.valueOf(status));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (throwable != null) {
                                Log.e(LOG_TAG, "Connection error 1.", throwable);
                            }else{
                                text.setText(status.toString());
                            }

                            if (status == AWSIotMqttClientStatus.Connected){
                                subscribe(topicMatching);
                                subscribe(topicUpdatedlist);
                            }
                        }
                    });
                }
            });
        } catch (final Exception e) {
            Log.e(LOG_TAG, "Connection error.", e);
            text.setText("Error! " + e.getMessage());
        }

    }

    @Override
    protected void onDestroy() {

        try {
            mqttManager.disconnect();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Disconnect error.", e);
        }

        super.onDestroy();
    }

    public void subscribe(String topic) {

        Log.d(LOG_TAG, "topic = " + topic);

        try {
            mqttManager.subscribeToTopic(topic, AWSIotMqttQos.QOS0,
                    new AWSIotMqttNewMessageCallback() {
                        @Override
                        public void onMessageArrived(final String topic, final byte[] data) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        String title = "";
                                        if (topic.equals(topicMatching)){
                                            title = "matching";
                                        }else if (topic.equals(topicUpdatedlist)){
                                            title = "updated list";
                                        }
                                        String message = new String(data, "UTF-8");

                                        Random rand = new Random();
                                        Log.e(LOG_TAG, title+rand.nextInt()+": "+message);
                                        txtMessage.setText(title+rand.nextInt()+": "+message);

                                    } catch (UnsupportedEncodingException e) {
                                        Log.e(LOG_TAG, "Message encoding error.", e);
                                    }
                                }
                            });
                        }
                    });
        } catch (Exception e) {
            Log.e(LOG_TAG, "Subscription error.", e);
        }
    }
}
